#version 400 core

in vec4 fragPosEye;
out vec4 fColor;

uniform vec3 color;

uniform bool hasFog;
uniform vec3 fogColor;
uniform float fogDensity;

float getFog(){
    float fragmentDistance = length(fragPosEye);
    float fogFactor = exp(-pow(fragmentDistance * fogDensity, 1));
    return clamp(fogFactor, 0.0f, 1.0f);
}

void main() {   

    fColor = vec4(color, 1.0f);
     if (hasFog){      
        float fogFactor = getFog();
        fColor = vec4(fogColor, 1.0f) * (1 - fogFactor) + vec4(color, 1.0f) *  fogFactor;
    }
}