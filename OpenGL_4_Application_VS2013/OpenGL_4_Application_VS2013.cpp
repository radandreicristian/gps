//Rad Andrei-Cristian, 30432
#define GLEW_STATIC


//Includes
#include <iostream>
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/matrix_inverse.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "GLEW/glew.h"
#include "GLFW/glfw3.h"
#include <string>
#include "Shader.hpp"
#include "Camera.hpp"
#include "SkyBox.hpp"
#define TINYOBJLOADER_IMPLEMENTATION

#include "Model3D.hpp"
#include "Mesh.hpp"

#pragma comment(lib, "legacy_stdio_definitions.lib")

//Global variables

//lightning
glm::vec3 pLightPos;
glm::vec3 pLightCol;
float cons, lin, quad;

int glWindowWidth = 1024;
int glWindowHeight = 768;
int retina_width, retina_height;
GLFWwindow* glWindow = NULL;

//uniforms
glm::mat4 model;
GLuint modelLoc;
glm::mat4 view;
GLuint viewLoc;
glm::mat4 projection;
GLuint projectionLoc;
glm::mat3 normalMatrix;
GLuint normalMatrixLoc;

glm::vec3 lightDir;
GLuint lightDirLoc;
glm::vec3 lightColor;
GLuint lightColorLoc;
glm::vec3 lightSpace;

//uniform mat
glm::mat3 lightDirMatrix;
GLuint lightDirMatrixLoc;
glm::mat4 lightSpaceMatrix;
GLuint lightSpaceMatrixLoc;

//boat position 

gps::Camera myCamera(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, -10.0f));
float cameraSpeed = 1.0f;

bool pressedKeys[1024];
float fieldOfView = 45.0f;

//shadows
const GLuint SHADOW_WIDTH = 2048;
const GLuint SHADOW_HEIGHT = 2048;

GLfloat lightAngle = 19.5f;
float boatAngle = 0.1f; //boat rotation angle

//skybox
gps::SkyBox skybox;
std::vector<const GLchar*> faces;

//shadows and fbos
GLuint depthMapFBO;
GLuint depthMapTexture;

//shaders
gps::Shader objectShader;
gps::Shader skyboxShader;
gps::Shader depthMapShader;
gps::Shader lightShader;

bool mouseFirst = true;
double xPrev = 0, yPrev = 0, yaw = 0, pitch = 0;
float sens = 0.1f;

//fog
bool hasFog = false;
glm::vec3 fogColor = glm::vec3(0.5f, 0.5f, 0.5f);
float fogDensity = 0.005;

//models
gps::Model3D island;
gps::Model3D ocean;
gps::Model3D boat;
gps::Model3D boat2;
gps::Model3D firstTent;
gps::Model3D secondTent;
gps::Model3D fireplace;
gps::Model3D human1;
gps::Model3D human2;
gps::Model3D lightCube;

bool wireframe = false;
bool boatStarted = false;

glm::vec3 boatPos = glm::vec3(50.0f, -10.0f, 360.0f);
glm::vec3 boat2Pos = glm::vec3(50.0f, -10.0f, 500.0f);
glm::mat4 boatModel = glm::translate(glm::mat4(1.0f), glm::vec3(50.0f, 0.0f, 320.0f));
float accel = 0.1f;
//functions for moving the boat
void moveBoatF() {
	float newX = boatPos.x + 0.5f;
	boatPos.x = newX;
}

void moveBoatB() {
	float newX = boatPos.x - 0.5f;
	boatPos.x = newX;
}


//done
GLenum glCheckError_(const char *file, int line)
{
	GLenum errorCode;
	while ((errorCode = glGetError()) != GL_NO_ERROR)
	{
		std::string error;
		switch (errorCode)
		{
		case GL_INVALID_ENUM:                  error = "INVALID_ENUM"; break;
		case GL_INVALID_VALUE:                 error = "INVALID_VALUE"; break;
		case GL_INVALID_OPERATION:             error = "INVALID_OPERATION"; break;
		case GL_STACK_OVERFLOW:                error = "STACK_OVERFLOW"; break;
		case GL_STACK_UNDERFLOW:               error = "STACK_UNDERFLOW"; break;
		case GL_OUT_OF_MEMORY:                 error = "OUT_OF_MEMORY"; break;
		case GL_INVALID_FRAMEBUFFER_OPERATION: error = "INVALID_FRAMEBUFFER_OPERATION"; break;
		}
		std::cout << error << " | " << file << " (" << line << ")" << std::endl;
	}
	return errorCode;
}
#define glCheckError() glCheckError_(__FILE__, __LINE__)

//done
void windowResizeCallback(GLFWwindow* window, int width, int height)
{
	fprintf(stdout, "window resized to width: %d , and height: %d\n", width, height);
	//TODO
	//for RETINA display
	glfwGetFramebufferSize(glWindow, &retina_width, &retina_height);

	objectShader.useShaderProgram();

	//set projection matrix
	glm::mat4 projection = glm::perspective(glm::radians(45.0f), (float)retina_width / (float)retina_height, 0.1f, 1000.0f);
	//send matrix data to shader
	GLint projLoc = glGetUniformLocation(objectShader.shaderProgram, "projection");
	glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));

	lightShader.useShaderProgram();

	glUniformMatrix4fv(glGetUniformLocation(lightShader.shaderProgram, "projection"), 1, GL_FALSE, glm::value_ptr(projection));

	//set Viewport transform
	glViewport(0, 0, retina_width, retina_height);
}

//TODO
void keyboardCallback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	//Window Close
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

	if (key >= 0 && key < 1024)
	{
		if (action == GLFW_PRESS)
			pressedKeys[key] = true;
		else if (action == GLFW_RELEASE)
			pressedKeys[key] = false;
	}

	//Fog enable/disable
	if (key == GLFW_KEY_F && action == GLFW_PRESS) {
		hasFog = !hasFog;
		
		objectShader.useShaderProgram();
		glUniform1i(glGetUniformLocation(objectShader.shaderProgram, "hasFog"), int(hasFog));

		skyboxShader.useShaderProgram();
		glUniform1i(glGetUniformLocation(skyboxShader.shaderProgram, "hasFog"), int(hasFog));

		lightShader.useShaderProgram();
		glUniform1i(glGetUniformLocation(lightShader.shaderProgram, "hasFog"), int(hasFog));

	}

	//Fog density increase
	if (pressedKeys[GLFW_KEY_N] && action == GLFW_PRESS) {
		//default density - 0.005
		fogDensity = glm::min(fogDensity + 0.001f, 0.01f);

		objectShader.useShaderProgram();
		glUniform1f(glGetUniformLocation(objectShader.shaderProgram, "fogDensity"), fogDensity);

		lightShader.useShaderProgram();
		glUniform1f(glGetUniformLocation(lightShader.shaderProgram, "fogDensity"), fogDensity);

	}

	//Fog density decrease
	if (pressedKeys[GLFW_KEY_M] && action == GLFW_PRESS) {
		fogDensity = glm::max(fogDensity - 0.001f, 0.001f);

		objectShader.useShaderProgram();
		glUniform1f(glGetUniformLocation(objectShader.shaderProgram, "fogDensity"), fogDensity);

		lightShader.useShaderProgram();
		glUniform1f(glGetUniformLocation(lightShader.shaderProgram, "fogDensity"), fogDensity);
	}

	//Wireframe enabling (although no use)
	if (pressedKeys[GLFW_KEY_P] && action == GLFW_PRESS) {
		wireframe != wireframe;
		if (wireframe) {
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		}
		else {
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		}
	}

	//Start the boat
	if (pressedKeys[GLFW_KEY_1] && action == GLFW_PRESS) {
		boatStarted = true;
	}

	//Reset boat position
	if (pressedKeys[GLFW_KEY_R] && action == GLFW_PRESS) {
		boatStarted = false;
		boatPos = glm::vec3(50.0f, 0.0f, 320.0f);
		accel = 0.001;

	}
}

//done
void mouseCallback(GLFWwindow* window, double xpos, double ypos){
	
	if (mouseFirst) {
		xPrev = xpos;
		yPrev = ypos;
		mouseFirst = false;
	}

	double xDiff = sens*(xpos - xPrev);
	double yDiff = sens*(ypos - yPrev);
	xPrev = xpos;
	yPrev = ypos;
	yaw += xDiff;
	pitch -= yDiff;
	if (pitch < -89.9f) {
		pitch = -89.9f;
	}
	if (pitch > 89.9f) {
		pitch = 89.9f;
	}

	myCamera.rotate(pitch, yaw);
}

//done
void scrollCallback(GLFWwindow* window, double xoffset, double yoffset)
{
	if (fieldOfView >= 1.0f && fieldOfView <= 45.0f)
		fieldOfView -= yoffset;
	if (fieldOfView <= 1.0f)
		fieldOfView = 1.0f;
	if (fieldOfView >= 45.0f)
		fieldOfView = 45.0f;
	projection = glm::perspective(float(glm::radians(fieldOfView)), float(retina_width) / float(retina_height), 0.1f, 1000.0f);
}

//TODO
void processKeyPress()
{

	if (pressedKeys[GLFW_KEY_W]) {
		myCamera.move(gps::MOVE_FORWARD, cameraSpeed);
	}

	if (pressedKeys[GLFW_KEY_S]) {
		myCamera.move(gps::MOVE_BACKWARD, cameraSpeed);
	}

	if (pressedKeys[GLFW_KEY_A]) {
		myCamera.move(gps::MOVE_LEFT, cameraSpeed);
	}

	if (pressedKeys[GLFW_KEY_D]) {
		myCamera.move(gps::MOVE_RIGHT, cameraSpeed);
	}

	if (pressedKeys[GLFW_KEY_UP]) {
		moveBoatF();
	}

	if (pressedKeys[GLFW_KEY_DOWN]) {
		moveBoatB();
	}

	//Lightning cube rotation
	if (pressedKeys[GLFW_KEY_J]) {
		lightAngle += 0.3f;
		if (lightAngle > 360.0f) {
			lightAngle -= 360.0f;
		}
		glm::vec3 lightDirTr = glm::vec3(glm::rotate(glm::mat4(1.0f), glm::radians(lightAngle), glm::vec3(0.0f, 1.0f, 0.0f)) * glm::vec4(lightDir, 1.0f));
		objectShader.useShaderProgram();
		glUniform3fv(lightDirLoc, 1, glm::value_ptr(lightDirTr));
	}

	//Lightning cube rotation
	if (pressedKeys[GLFW_KEY_K]) {
		lightAngle -= 0.3f;
		if (lightAngle < 0.0f) {
			lightAngle += 360.0f;
		}
		glm::vec3 lightDirTr = glm::vec3(glm::rotate(glm::mat4(1.0f), glm::radians(lightAngle), glm::vec3(0.0f, 1.0f, 0.0f)) * glm::vec4(lightDir, 1.0f));
		objectShader.useShaderProgram();
		glUniform3fv(lightDirLoc, 1, glm::value_ptr(lightDirTr));
	}

}

//done
bool initOpenGLWindow()
{
	if (!glfwInit()) {
		fprintf(stderr, "ERROR: could not start GLFW3\n");
		return false;
	}

	//for Mac OS X
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	glWindow = glfwCreateWindow(glWindowWidth, glWindowHeight, "OpenGL Shader Example", NULL, NULL);
	if (!glWindow) {
		fprintf(stderr, "ERROR: could not open window with GLFW3\n");
		glfwTerminate();
		return false;
	}

	glfwSetWindowSizeCallback(glWindow, windowResizeCallback);
	glfwMakeContextCurrent(glWindow);

	glfwWindowHint(GLFW_SAMPLES, 4);

	// start GLEW extension handler
	glewExperimental = GL_TRUE;
	glewInit();

	// get version info
	const GLubyte* renderer = glGetString(GL_RENDERER); // get renderer string
	const GLubyte* version = glGetString(GL_VERSION); // version as a string
	printf("Renderer: %s\n", renderer);
	printf("OpenGL version supported %s\n", version);

	//for RETINA display
	glfwGetFramebufferSize(glWindow, &retina_width, &retina_height);

	glfwSetKeyCallback(glWindow, keyboardCallback);
	glfwSetCursorPosCallback(glWindow, mouseCallback);
	glfwSetScrollCallback(glWindow, scrollCallback);
	glfwSetInputMode(glWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	return true;
}

//done
void initOpenGLState()
{
	glClearColor(0.5, 0.5, 0.5, 1.0);
	glViewport(0, 0, retina_width, retina_height);

	glEnable(GL_DEPTH_TEST); // enable depth-testing
	glDepthFunc(GL_LESS); // depth-testing interprets a smaller value as "closer"
	glEnable(GL_CULL_FACE); // cull face
	glCullFace(GL_BACK); // cull back face
	glFrontFace(GL_CCW); // GL_CCW for counter clock-wise
}

//done
glm::mat4 computeLightSpaceTrMatrix()
{
	const GLfloat near_plane = 1.0f, far_plane = 30.0f;
	glm::mat4 lightProjection = glm::ortho(-20.0f, 20.0f, -20.0f, 20.0f, near_plane, far_plane);
	glm::vec3 lightDirTr = glm::vec3(glm::rotate(glm::mat4(1.0f), glm::radians(lightAngle), glm::vec3(0.0f, 1.0f, 0.0f)) * glm::vec4(lightDir, 1.0f));
	glm::mat4 lightView = glm::lookAt(lightDirTr, myCamera.getCameraTarget(), glm::vec3(0.0f, 1.0f, 0.0f));
	return lightProjection * lightView;
}

//done
void initFBOs() {

	glGenFramebuffers(1, &depthMapFBO);
	glGenTextures(1, &depthMapTexture);
	glBindTexture(GL_TEXTURE_2D, depthMapTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMapTexture, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

//done
void initSkybox() {
	faces.push_back("skybox/DaylightBox_Right.bmp");
	faces.push_back("skybox/DaylightBox_Left.bmp");
	faces.push_back("skybox/DaylightBox_Top.bmp");
	faces.push_back("skybox/DaylightBox_Bottom.bmp");
	faces.push_back("skybox/DaylightBox_Front.bmp");
	faces.push_back("skybox/DaylightBox_Back.bmp");
}

//TODO
void initModels()
{
	skybox.Load(faces);
	island = gps::Model3D("objects/tropical/Small Tropical Island.obj", "objects/tropical/");
	ocean = gps::Model3D("objects/ocean2/Ocean.obj", "objects/ocean2/");
	boat = gps::Model3D("objects/Boat9/boat.obj", "objects/Boat9/");
	boat2 = gps::Model3D("objects/Boat9/boat.obj", "objects/Boat9/");
	firstTent = gps::Model3D("objects/Tent/10495_Green_Mesh_Tent_V1_L3.obj", "objects/Tent/");
	secondTent = gps::Model3D("objects/Tent/10495_Green_Mesh_Tent_V1_L3.obj", "objects/Tent/");
	fireplace = gps::Model3D("objects/Campfire3/Campfire_v2.obj", "objects/Campfire3/");
	human1 = gps::Model3D("objects/Human2/muro.obj", "objects/Human2/");
	human2 = gps::Model3D("objects/Human2/muro.obj", "objects/Human2/");
	lightCube = gps::Model3D("objects/Cube/cube.obj", "objects/Cube/");
}

//done
void initShaders()
{
	objectShader.loadShader("shaders/shaderStart.vert", "shaders/shaderStart.frag");
	skyboxShader.loadShader("shaders/skyboxShader.vert", "shaders/skyboxShader.frag");
	depthMapShader.loadShader("shaders/simpleDepthMap.vert", "shaders/simpleDepthMap.frag");
	lightShader.loadShader("shaders/lightCube.vert", "shaders/lightCube.frag");
}

//TODO
void initUniforms()
{
	objectShader.useShaderProgram();

	model = glm::mat4(1.0f);
	modelLoc = glGetUniformLocation(objectShader.shaderProgram, "model");
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

	view = myCamera.getViewMatrix();
	viewLoc = glGetUniformLocation(objectShader.shaderProgram, "view");
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));

	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	normalMatrixLoc = glGetUniformLocation(objectShader.shaderProgram, "normalMatrix");
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));

	projection = glm::perspective(glm::radians(45.0f), (float)retina_width / (float)retina_height, 0.1f, 1000.0f);
	projectionLoc = glGetUniformLocation(objectShader.shaderProgram, "projection");
	glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));

	lightDir = glm::vec3(0.0f, 200.0f, 100.0f);
	lightDirLoc = glGetUniformLocation(objectShader.shaderProgram, "lightDir");
	glUniform3fv(lightDirLoc, 1, glm::value_ptr(lightDir));
	
	lightDirMatrixLoc = glGetUniformLocation(objectShader.shaderProgram, "lightDirMatrix");
	glUniformMatrix3fv(lightDirMatrixLoc, 1, GL_FALSE, glm::value_ptr(lightDirMatrix));

	lightColor = glm::vec3(1.0f, 1.0f, 0.6f); //ligh yellow light
	lightColorLoc = glGetUniformLocation(objectShader.shaderProgram, "lightColor");
	glUniform3fv(lightColorLoc, 1, glm::value_ptr(lightColor));

	objectShader.useShaderProgram();
	glUniform1i(glGetUniformLocation(objectShader.shaderProgram, "hasFog"), int(hasFog));
	glUniform1f(glGetUniformLocation(objectShader.shaderProgram, "fogDensity"), fogDensity);
	glUniform3fv(glGetUniformLocation(objectShader.shaderProgram, "fogColor"), 1, glm::value_ptr(fogColor));

	skyboxShader.useShaderProgram();
	glUniform1i(glGetUniformLocation(skyboxShader.shaderProgram, "hasFog"), int(hasFog));
	glUniform3fv(glGetUniformLocation(skyboxShader.shaderProgram, "fogColor"), 1, glm::value_ptr(fogColor));

	lightShader.useShaderProgram();
	glUniform1i(glGetUniformLocation(lightShader.shaderProgram, "hasFog"), int(hasFog));
	glUniform1f(glGetUniformLocation(lightShader.shaderProgram, "fogDensity"), fogDensity);
	glUniform3fv(glGetUniformLocation(lightShader.shaderProgram, "fogColor"), 1, glm::value_ptr(fogColor));

	glUniformMatrix4fv(glGetUniformLocation(lightShader.shaderProgram, "projection"), 1, GL_FALSE, glm::value_ptr(projection));

}

//TODO
void renderSkybox() {
	glCullFace(GL_BACK);
	skyboxShader.useShaderProgram();
	glUniform3fv(glGetUniformLocation(skyboxShader.shaderProgram, "lightColor"), 1, glm::value_ptr(lightColor));
	skybox.Draw(skyboxShader, view, projection);
}

//TODO
void renderObjects() {
	//OBJECTS RENDERING

	//OCEAN
	objectShader.useShaderProgram();
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(0.0f, -6.0f, 0.0f));
	model = glm::scale(model, glm::vec3(1000.0f, 0.0f, 1000.0f));
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	view = myCamera.getViewMatrix();
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
	projection = glm::perspective(float(glm::radians(fieldOfView)), float(retina_width) / float(retina_height), 0.1f, 1000.0f);
	glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	ocean.Draw(objectShader);

	//ISLAND
	objectShader.useShaderProgram();
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(0.0f, -10.0f, 0.0f));
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	view = myCamera.getViewMatrix();
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
	projection = glm::perspective(float(glm::radians(fieldOfView)), float(retina_width) / float(retina_height), 0.1f, 1000.0f);
	glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	island.Draw(objectShader);

	//Tent1
	objectShader.useShaderProgram();
	model = glm::mat4(1.0f); 
	model = glm::translate(model, glm::vec3(-200.0f, 5.0f, 10.0f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::rotate(model, glm::radians(210.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	model = glm::scale(model, glm::vec3(0.45f, 0.45f, 0.45f));
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	view = myCamera.getViewMatrix();
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
	projection = glm::perspective(float(glm::radians(fieldOfView)), float(retina_width) / float(retina_height), 0.1f, 1000.0f);
	glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	firstTent.Draw(objectShader);

	//tent2
	objectShader.useShaderProgram();
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(-200.0f, 7.0f, 150.0f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::rotate(model, glm::radians(150.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	model = glm::scale(model, glm::vec3(0.5f, 0.5f, 0.5f));
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	view = myCamera.getViewMatrix();
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
	projection = glm::perspective(float(glm::radians(fieldOfView)), float(retina_width) / float(retina_height), 0.1f, 1000.0f);
	glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	secondTent.Draw(objectShader);

	//Fireplace
	objectShader.useShaderProgram();
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(-250.0f, 2.0f, 80.0f));
	model = glm::scale(model, glm::vec3(0.5f, 0.5f, 0.5f));
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	view = myCamera.getViewMatrix();
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
	projection = glm::perspective(float(glm::radians(fieldOfView)), float(retina_width) / float(retina_height), 0.1f, 1000.0f);
	glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fireplace.Draw(objectShader);

	//Human
	objectShader.useShaderProgram();
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(-310.0f, -2.0f, 30.0f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.5f, 0.5f, 0.5f));
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	view = myCamera.getViewMatrix();
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
	projection = glm::perspective(float(glm::radians(fieldOfView)), float(retina_width) / float(retina_height), 0.1f, 1000.0f);
	glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	human1.Draw(objectShader);

	//Human2
	objectShader.useShaderProgram();
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(190.0f, 95.0f, 80.0f));
	model = glm::scale(model, glm::vec3(0.5f, 0.5f, 0.5f));
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	view = myCamera.getViewMatrix();
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
	projection = glm::perspective(float(glm::radians(fieldOfView)), float(retina_width) / float(retina_height), 0.1f, 1000.0f);
	glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	human2.Draw(objectShader);

	//Boat
	objectShader.useShaderProgram();
	model = glm::mat4(1.0f);
	if (boatStarted) {
		accel += 0.002;
		boatPos.x += 0.1 + accel * accel;
	}
	model = glm::translate(model, boatPos);
	model = glm::rotate(model, glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.1f, 0.1f, 0.1f));
//	model = glm::rotate(model, rotAngle, glm::vec3(0.0f, 1.0f, 0.0f));
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	view = myCamera.getViewMatrix();
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
	projection = glm::perspective(float(glm::radians(fieldOfView)), float(retina_width) / float(retina_height), 0.1f, 1000.0f);
	glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	boat.Draw(objectShader);

	//Boat2
	objectShader.useShaderProgram();
	model = glm::mat4(1.0f);
	//boatAngle -= 0.1f;
//	if (boatAngle < 0.0f) {
	//	boatAngle += 360.0f;
	//}
	boat2Pos = glm::vec3(glm::rotate(glm::mat4(1.0f), glm::radians(boatAngle), glm::vec3(0.0f, 1.0f, 0.0f)) * glm::vec4(boat2Pos, 1.0f));
	model = glm::translate(model, boat2Pos);
	model = glm::rotate(model, glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.1f, 0.1f, 0.1f));
	//model = glm::rotate(model, boatAngle, glm::vec3(0.0f, 1.0f, 0.0f));
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	view = myCamera.getViewMatrix();
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
	projection = glm::perspective(float(glm::radians(fieldOfView)), float(retina_width) / float(retina_height), 0.1f, 1000.0f);
	glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	boat.Draw(objectShader);

}

//TODO
void renderLightCube(glm::vec3 color, glm::vec3 position) {

	glCullFace(GL_BACK);
	lightShader.useShaderProgram();
		
	view = myCamera.getViewMatrix();
	glUniformMatrix4fv(glGetUniformLocation(lightShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));

	projection = glm::perspective((float)glm::radians(fieldOfView), (float)retina_width / (float)retina_height, 0.1f, 1000.0f);
	glUniformMatrix4fv(glGetUniformLocation(lightShader.shaderProgram, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
	
	glUniform3fv(glGetUniformLocation(lightShader.shaderProgram, "color"), 1, glm::value_ptr(color));
	
	model = glm::rotate(glm::mat4(1.0f), glm::radians(lightAngle), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::translate(model, position);
	model = glm::scale(model, glm::vec3(2.5f, 2.5f, 2.5f));
	glUniformMatrix4fv(glGetUniformLocation(lightShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
	lightCube.Draw(lightShader);

}

//done
void renderDepthMap() {

	depthMapShader.useShaderProgram();

	glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "lightSpaceTrMatrix"),1, GL_FALSE, glm::value_ptr(computeLightSpaceTrMatrix()));
	glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glClear(GL_DEPTH_BUFFER_BIT);
	
	model = glm::mat4(1.0f); 
	model = glm::translate(model, glm::vec3(-310.0f, -2.0f, 30.0f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
	human1.Draw(depthMapShader);
	
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(190.0f, 95.0f, 80.0f));
	model = glm::scale(model, glm::vec3(0.5f, 0.5f, 0.5f));
	glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
	human2.Draw(depthMapShader);

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(-200.0f, 7.0f, 150.0f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::rotate(model, glm::radians(150.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	model = glm::scale(model, glm::vec3(0.5f, 0.5f, 0.5f));
	glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
	firstTent.Draw(depthMapShader);

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(-200.0f, 7.0f, 150.0f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::rotate(model, glm::radians(150.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	model = glm::scale(model, glm::vec3(0.5f, 0.5f, 0.5f));
	glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
	secondTent.Draw(depthMapShader);

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(-250.0f, 2.0f, 80.0f));
	model = glm::scale(model, glm::vec3(0.5f, 0.5f, 0.5f));
	glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
	fireplace.Draw(depthMapShader);

	model = glm::mat4(1.0f);
	model = glm::translate(model, boatPos);
	model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	//model = glm::rotate(model, rotAngle, glm::vec3(0.0f, 1.0f, 0.0f));
	glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
	boat.Draw(depthMapShader);

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(0.0f, -10.0f, 0.0f));
	glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
	island.Draw(depthMapShader);

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(0.0f, -6.0f, 0.0f));
	model = glm::scale(model, glm::vec3(10000.0f, 0.0f, 10000.0f));
	glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
	ocean.Draw(depthMapShader);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

}

//Todo
void renderScene()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	processKeyPress();
	
	//render depth map - first pass
	renderDepthMap();

	objectShader.useShaderProgram();

	glUniformMatrix4fv(glGetUniformLocation(objectShader.shaderProgram, "lightSpaceTrMatrix"), 1, GL_FALSE, glm::value_ptr(computeLightSpaceTrMatrix()));

	view = myCamera.getViewMatrix();
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));

	lightDirMatrix = glm::mat3(glm::inverseTranspose(view));
	glUniformMatrix3fv(lightDirMatrixLoc, 1, GL_FALSE, glm::value_ptr(lightDirMatrix));

	glViewport(0, 0, retina_width, retina_height);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, depthMapTexture);
	glUniform1i(glGetUniformLocation(objectShader.shaderProgram, "shadowMap"), 3);

	renderSkybox();

	//render objects - second pass
	renderObjects();

	renderLightCube(lightColor, lightDir);
}

int main(int argc, const char * argv[]) {

	initOpenGLWindow();
	initOpenGLState();
	initFBOs();
	initSkybox();
	initModels();
	initShaders();
	initUniforms();	
	
	while (!glfwWindowShouldClose(glWindow)) {
		renderScene();
		glfwPollEvents();
		glfwSwapBuffers(glWindow);
	}

	glfwTerminate();

	return 0;
}
